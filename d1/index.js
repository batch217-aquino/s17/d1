// Functions

function printName(){
    
    console.log("My name is John")
};
printName();

function declaredFunction(){
    console.log("Hello World from declaredFunction.")
}
declaredFunction();

// Function Expression

let variableFunction = function(){
    console.log("Hello Again!");
}
variableFunction();
console.log(typeof variableFunction)

let funcExpression = function funcName(){
    console.log("Hello from the other side.");
}
funcExpression();

funcExpression = function(){
    console.log("Updated funcExpression")
}
funcExpression();
funcExpression();
funcExpression();

// updated declaredFunction

declaredFunction = function(){
    console.log("Updated declaredFuntion")
}
declaredFunction();


// Re-Assigning function declared const

const constantFunc = function(){
    console.log("Initailized with const!")
}
constantFunc();

// constantFunc = function(){
//     console.log("Re-assign it?")
// }
// constantFunc();


// Function Scoping

{
    let localVar = "Armando Perez"
    console.log(localVar)
}

let globalScope = "Mr. WorldWide";
console.log(globalScope)

// Function Scoping
function showName(){
    // Function scoped Variables 
    const functionConst = "John";
    let functionLet ="Jane";
    console.log(functionConst)
    console.log(functionLet)
}
showName();

// Nested Function

function myNewFunction(){
    let name = "Jane";

    function nestedFunction(){
        let nestedName = "John";
        console.log(name);
    }
    nestedFunction();
}
myNewFunction();


// Function and Global Scope Variable
// Global Scope Variable
let globalName = "Alexandro";
function myNewFunction2(){
    let nameInside = "Renz";
    console.log(globalName)
}
myNewFunction2();
// console.log(nameInside)

// Using alert 
alert("Hello World!"); // Executed immediatly

function showAlert(){
    alert("Hello World!")
}
showAlert();
console.log("I will only log in the console when alert");

// Using Prompt

let samplePrompt = prompt("Enter your name.");
console.log("Hello "+ samplePrompt );
console.log(typeof samplePrompt );

function printWelcomeMsg(){
    let firstName = prompt("Enter your first name: ")
    let lastName = prompt("Enter your last name: ")
    console.log("Hello, " +firstName+" "+lastName)
    console.log("Welcome to my page!")
}
printWelcomeMsg();




// Function Naming Convention
function getCourse(){
    let courses = ["Science 101", "Math 101", "English 101"];
    console.log(courses);
}
getCourse();

function get(){
    let name = "Jamie";
    console.log(name)
}
get()

function foo(){
    console.log(25%5)
}
foo();

function displayCarInfo(){
    console.log("Brand: Toyota");
    console.log("Type: Sedan");
    console.log("Price: 1,500,000");
}
displayCarInfo()