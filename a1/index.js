/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function basicInfo(){
        let fullName = prompt("Enter your Full Name: ");
        let age = prompt("Enter your Age: ");
        let address = prompt("Enter your address: ");
        console.log("Hello, " + fullName);
        console.log("You are "+ age + " years old.");
        console.log("You lived in "+ address);
    }
    basicInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function favBand(){
        console.log("1. Parokya ni Edgar");
        console.log("2.Eraser Head");
        console.log("3. Westlife");
        console.log("4. The Beatles");
        console.log("5. Silent Sanctuary");
    }
    favBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function favMovies(){
        console.log("1. The Lord of the Rings: The Fellowship of the Ring Raing: 88%");
        console.log("2.Gladiator Rating: 85%");
        console.log("3. Avengers: Infinity War Rating: 84%");
        console.log("4. Like Stars on Earth Rating 82%");
        console.log("5. Spider-Man: No Way Home Rating 82%");
    }
    favMovies();



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
    alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
    
	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printFriends();

// console.log(friend1);
// console.log(friend2);
